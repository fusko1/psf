# first of all import the socket library 
import socket			 

# next create a socket object 
s = socket.socket()		 
print( "Socket successfully created")

# reserve a port on your computer in our 
# case it is 12345 but it can be anything 
port = 122				

# Next bind to the port 
# we have not typed any ip in the ip field 
# instead we have inputted an empty string 
# this makes the server listen to requests 
# coming from other computers on the network 
s.bind(('', port))		 
print ("socket binded to %s" %(port))

# put the socket into listening mode 
s.listen(5)	 
print ("socket is listening")			
users=[()] 
# a forever loop until we interrupt it or 
# an error occurs 

while True: 

   # Establish connection with client. 

   c, addr = s.accept()	 
   users= {addr:c}
   print(users)
 
   msg=(c.recvmsg(1024))
   print( "Got connection from2", msg)
   if(msg=='stop'):
      c.close()
      break
   c=users.get(addr,'null')
   # send a thank you message to the client. 
   c.send(("Thank you for connecting").encode()) 

   # Close the connection with the client 
s.stop()
